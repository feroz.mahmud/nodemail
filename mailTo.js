/**
 * Created by MDFEROZ on 01-06-2019.
 */
var mailer = require("nodemailer");


exports.sendTo=function (req,res) {


// Use Smtp Protocol to send Email
    var smtpTransport = mailer.createTransport({
        host: 'smtp.gmail.com',
        port: 465,
        secure: true,
        auth: {
            user: 'xxx@gmail.com',
            pass: 'xxx'
        }
    });

    res.render('sendMailTo',{title:'SmartTech Pvt. Ltd'},function (err,data) {
        if (err) {
            console.log(err);
        } else {
            var mainOptions = {
                from: "atlasbdit@gmail.com",
                to: "btl.bridgetech@gmail.com,feroz9915@gmail.com",
                subject: "Send Email Using Node.js",
                html: data
            };
            console.log("html data ======================>", mainOptions.html);
            smtpTransport.sendMail(mainOptions, function (err, info) {
                if (err) {
                    console.log(err);
                } else {
                    console.log('Message sent: ' + info.response);
                }
                smtpTransport.close();
            });
        }
    });
}